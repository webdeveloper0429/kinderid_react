export const geolocationOption={
    enableHighAccuracy: false,
    timeout: 5000,
    maximumAge: 0
}

export const PRICE={
    ONE_YEAR: 249,
    THREE_YEAR: 499
}