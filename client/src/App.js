import React, {Component} from 'react';
import {Router, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {register_socket} from './actions/index';
import {registerSocket} from './services/registerSocket';

import createRoutes from './routes'

const routes = createRoutes();

class App extends Component{

    // componentWillMount(){
    //     registerSocket(this)
    // }
    render(){
        return(
            <Router history={browserHistory}>
                {routes}
            </Router>
        )
    }
}

const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        register_socket: register_socket
    }, dispatch)
}
export default connect(null, mapDispatchToProps)(App)