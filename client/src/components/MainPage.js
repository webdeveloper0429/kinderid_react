import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { fetchUserData, fetchSubscriptions } from '../actions/userActions';

class MainPage extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  componentWillMount() {
    this.getUser();
  }
  getUser = async () => {
    await this.props.fetchUserData();
    await this.props.fetchSubscriptions();
    console.log('Artem this is your data', this.props.subscriptions);
  };

  goBack = () => {
    browserHistory.goBack();
  };
  onSignOut(){
    localStorage.removeItem('kinderIdAccessToken');
    browserHistory.push('/');
  }
  render() {
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container ">
            <h4 style={{ paddingBottom: '4rem' }}>Welcome, {this.props.parent.user? this.props.parent.user.parentName : ''}</h4>
            <div className="row">
              <div className="col-12 col-md-6">
                <table className="table">
                  <thead>
                    <tr>
                      <th>ACTIVE</th>
                      <th />
                      <th />
                      <th />
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.subscriptions.map((subscription, index)=>{
                        return(
                          <tr key={index} >
                            <td className="field-name">{subscription.childrenName}</td>
                            <td>{subscription.days_left} Days left</td>
                            <td>{subscription.wristband}</td>
                            <td>
                              <a>
                                <img alt="#" src="images/arrow-color.png" className="arrow-icon" />
                              </a>
                            </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
                {/* <table className="table">
                  <thead>
                    <tr>
                      <th>INACTIVE</th>
                      <th />
                      <th />
                      <th />
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="field-name">Michael </td>
                      <td>29 Days left</td>
                      <td>AB 000000</td>
                      <td>
                        <a>
                          <img alt="#" src="images/arrow-color.png" className="arrow-icon" />
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table> */}
              </div>
              <div className="col-12 col-md-4 ml-auto">
                <table className="table">
                  <thead>
                    <tr>
                      <th>SETTINGS</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="field-name">
                        <a href="" onClick={() => browserHistory.push('EditProfile')}>
                          Edit profile
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td className="field-name">
                        <a href="" onClick={() => browserHistory.push('FAQ')}>FAQ</a>
                      </td>
                    </tr>
                    <tr>
                      <td className="field-name">
                        <a href="" onClick={() => browserHistory.push('Contact')}>Support</a>
                      </td>
                    </tr>
                    <tr>
                      <td className="field-name">
                        <a href="" onClick={() => browserHistory.push('Legal')}>Legal</a>
                      </td>
                    </tr>
                    <tr>
                      <td className="field-name">
                        <a href="" onClick={() => this.onSignOut()}>Sign out</a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { parent, subscriptions } = user;
  return { parent, subscriptions };
};

export default connect(mapStateToProps, { fetchUserData, fetchSubscriptions })(MainPage);
