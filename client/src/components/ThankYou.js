import React, { Component } from 'react';
import Header from './layouts/Header';
import { connect } from 'react-redux';
import { checkEmail } from '../actions/authActions';

class Signin extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
        };
    }

    handleChange = event => {
        this.setState({ email: event.target.value });
    };

    // handleSubmit = async event => {
    //     event.preventDefault();
    //     await this.props.checkEmail(this.state.email);
    //     const { route } = this.props;
    //     console.log('message', route);

    //     if (route === 'new_user') {
    //         return this.props.history.push('PersonalInfo');
    //     }
    // };

    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container text-center">
                        <h4>Thank you for helping</h4>
                        <p>
                            We would like to thank you by offering a 20%<br />
                            discount on our subscribtion so you can use our<br />
                            services for you own children.
                        </p>
                        <p>
                            Please enter your email address to<br />
                            receive your discount.
                        </p>
                        <div className="select-group select-group2" style={{ marginTop: 0 }}>
                            <div className="form-line">
                                <div className="wrap-input">
                                    <input
                                        type="email"
                                        value={this.state.email}
                                        onChange={this.handleChange}
                                        placeholder="example@mail.com"
                                        required="true"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="navigation text-center">
                            <a href="/">
                                Next <i className="fa fa-angle-right" aria-hidden="true" />
                            </a>
                        </div>
                    </div>
                </main>
                <img alt="logo" src="images/step4.jpg" className="step_img" style={{width: '100%'}} />
            </div>
        );
    }
}
const mapStateToProps = ({ auth }) => {
    const { email, route } = auth;
    return { email, route };
};

export default connect(mapStateToProps, { checkEmail })(Signin);
