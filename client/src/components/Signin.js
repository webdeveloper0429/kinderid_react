import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { checkEmail } from '../actions/authActions';

class Signin extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
    };
  }

  goBack = () => {
    browserHistory.goBack();
  };

  handleChange = event => {
    this.setState({ email: event.target.value });
  };

  handleSubmit = async event => {
    event.preventDefault();
    await this.props.checkEmail(this.state.email);
    const { route, email } = this.props;
    console.log('email', email);
    if (route === 'new_user') {
      return browserHistory.push('PersonalInfo');
    }
    if (route === 'authorized_email') {
      return browserHistory.push('Password');
    }
    return null;
  };

  render() {
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Email address</h4>
            <p>
              If you have previously registered use<br />
              the same address.
            </p>
            <div className="select-group select-group2">
              <div className="form-line">
                <form onSubmit={this.handleSubmit}>
                  <div className="wrap-input">
                    <input
                      type="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                      placeholder="example@mail.com"
                      required="true"
                    />
                  </div>
                </form>
              </div>
            </div>
            <div className="navigation text-center">
              <a onClick={this.goBack}>
                <i className="fa fa-angle-left" aria-hidden="true" /> Back
              </a>
              {
                this.state.email?
                <a onClick={this.handleSubmit}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
                :null
              }              
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ auth }) => {
  const { email, route } = auth;
  return { email, route };
};

export default connect(mapStateToProps, { checkEmail })(Signin);
