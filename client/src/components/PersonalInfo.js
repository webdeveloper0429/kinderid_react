import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Header from './layouts/Header';
import { connect } from 'react-redux';
import { storeData } from '../actions/authActions';
import ReactPhoneInput from 'react-phone-input-2';
import ReactGoogleMapLoader from "react-google-maps-loader"
import ReactGooglePlacesSuggest from "react-google-places-suggest"
const MY_API_KEY = "AIzaSyDJcWRkEzgEVgkeQfP7oHMOc1zvvSuQVmk" 

class PersonalInfo extends Component {
  constructor(props){
    super(props);
    this.mobileChange = this.mobileChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSelectSuggest = this.handleSelectSuggest.bind(this);
    this.state = {
        search: "",
        value: "",
    }
  }
  goBack = () => {
    this.props.history.goBack();
  };

  handleSubmit = event => {
    event.preventDefault();
    return browserHistory.push('ChoosePassword');
  };

  mobileChange(value) {
    this.props.storeData({ prop: 'mobile', value });
  }

  handleChange = event => {
    const { name, value } = event.target;
    this.props.storeData({ prop: name, value });
  };
  handleInputChange(e) {
    this.setState({search: e.target.value, value: e.target.value})
  }

  handleSelectSuggest(suggest) {
    console.log(suggest)
    this.setState({search: "", value: suggest.formatted_address})
    let address = suggest.formatted_address;
    this.props.storeData({ prop: 'address', value: address });
    suggest.address_components.map(address_component=>{
      address_component.types.map(type=>{
        if(type === 'postal_code'){
          let post = address_component.long_name;
          this.props.storeData({ prop: 'post', value: post });
        }
        if(type === "postal_town"){
          let city = address_component.long_name;
          this.props.storeData({ prop: 'city', value: city });
        }
        return true;
      })
      return true;
    })

  }
  render() {
    const valid = this.props.fullName && this.props.mobile
                  && this.props.address 

    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Personal information</h4>
            <p>Enter the required information.</p>
            <div className="select-group select-group2" style={{ marginTop: '2em' }}>
              <div className="form-line form-line2">
                <form onSubmit={this.handleSubmit}>
                  <label>Full name</label>
                  <div className="wrap-input ">
                    <input
                      type="text"
                      name="fullName"
                      onChange={this.handleChange}
                      value={this.props.fullName}
                      placeholder="Michael Smith"
                    />
                  </div>
                  <label>Telephone number</label>
                  <div className="wrap-input ">
                    <div className="center_phone_input">
                      <ReactPhoneInput
                          defaultCountry={'no'}
                          value={this.props.mobile}
                          inputStyle={{
                              border: 'none',
                              width: 'inherit',
                              paddingRight: 30,
                              fontSize: 18
                          }}
                          buttonStyle={{
                              border: 'none',
                              background: 'transparent'
                          }}
                          onChange={this.mobileChange}
                      />
                    </div>                    
                  </div>
                  <label>Address</label>
                  <div className="wrap-input ">
                  <ReactGoogleMapLoader
                    params={{
                        key: MY_API_KEY,
                        libraries: "places,geocode",
                    }}
                    render={googleMaps =>
                        googleMaps && (
                            <ReactGooglePlacesSuggest
                                googleMaps={googleMaps}
                                autocompletionRequest={{
                                    input: this.state.search,
                                    // Optional options
                                    // https://developers.google.com/maps/documentation/javascript/reference?hl=fr#AutocompletionRequest
                                }}
                                // Optional props
                                onSelectSuggest={this.handleSelectSuggest}
                                textNoResults="My custom no results text" // null or "" if you want to disable the no results item
                                customRender={prediction => (
                                    <div className="customWrapper">
                                        {prediction
                                            ? prediction.description
                                            : "My custom no results text"}
                                    </div>
                                )}
                            >
                                <input
                                    type="text"
                                    value={this.state.value}
                                    placeholder="Dalsbergsiten 6A"
                                    onChange={this.handleInputChange}
                                />
                            </ReactGooglePlacesSuggest>
                        )
                    }
                  />                  
                  </div>
                  <label>Postal code</label>
                  <div className="wrap-input ">
                    <input
                      type="text"
                      name="post"
                      value={this.props.post}
                      onChange={this.handleChange}
                      placeholder="0170"
                    />
                  </div>
                  <label>City</label>
                  <div className="wrap-input ">
                    <input
                      type="text"
                      name="city"
                      value={this.props.city}
                      onChange={this.handleChange}
                      placeholder="OSLO"
                    />
                  </div>
                </form>
              </div>
            </div>
            <div className="navigation text-center normal-postion">
              <a onClick={this.goBack}>
                <i className="fa fa-angle-left" aria-hidden="true" /> Back
              </a>
              {
                valid?
                <a onClick={this.handleSubmit}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
                :null
              }              
            </div>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { fullName, mobile, address, post, city } = auth;
  return {
    fullName,
    mobile,
    address,
    post,
    city,
  };
};

export default connect(mapStateToProps, { storeData })(PersonalInfo);
