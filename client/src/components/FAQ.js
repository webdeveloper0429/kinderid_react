import React, { Component } from 'react';
import Header from './layouts/Header';

class FAQ extends Component {
    constructor() {
        super();
        this.state = {};
    }
    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container ">
                        <div className="row">
                            <div className="col-12 col-md-9 col-lg-10 order-1 order-md-2">
                                <h4>Legal 1</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas purus nec tellus ornare
                                    pulvinar. Maecenas ut arcu in neque euismod congue ut at turpis. In hac habitasse platea dictumst.</p>

                                <p>Aliquam ut libero a eros pharetra condimentum eu non magna. Vestibulum non interdum ligula,
                                    sed tempus est. Maecenas porttitor erat sed est sagittis, quis euismod nisi lobortis. Pellentesquene
                                    maximus dolor lorem, malesuada suscipit magna tincidunt et. Aliquam ac neque et dui egestasakon
                                    incidunt a id mi. Duis maximus viverra arcu, non viverra metus ultrices non. Cras congue tortoror
                                    tincidunt varius. Ut venenatis convallis eros, ut auctor dolor interdum ac. Pellentesque habitant morbi
                                    tristique senectus et netus et malesuada fames ac turpis egestas. Praesent in malesuada justo, vitae
                                    venenatis eros. Suspendisse potenti.</p>

                                <p><strong>Title 2</strong><br/>
                                    Aliquam ut libero a eros pharetra condimentum eu non magna. Vestibulum non interdum ligula,
                                    sed tempus est. Maecenas porttitor erat sed est sagittis, quis euismod nisi lobortis. Pellentesquene
                                    maximus dolor lorem, malesuada suscipit magna tincidunt et. Aliquam ac neque et dui egestasakon
                                    incidunt a id mi. Duis maximus viverra arcu, non viverra metus ultrices non. Cras congue tortoror
                                    tincidunt varius. Ut venenatis convallis eros, ut auctor dolor interdum ac. Pellentesque habitant morbi
                                    tristique senectus et netus et malesuada fames ac turpis egestas. Praesent in malesuada justo, vitae
                                    venenatis eros. Suspendisse potenti</p>
                            </div>
                            <div className="col-12 col-md-3 col-lg-2 order-2 order-md-1">
                                <h3 className="widget-title">Articles</h3>
                                <ul className="list-link">
                                    <li className="active"><a href="">Legal 1</a></li>
                                    <li><a href="">Eksempel</a></li>
                                    <li><a href="">Eksempel</a></li>
                                    <li><a href="">Eksempel</a></li>
                                    <li><a href="">Eksempel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default FAQ;
