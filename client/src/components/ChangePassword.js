import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { checkEmail } from '../actions/authActions';

class ChangePassword extends Component {
  goBack = () => {
    browserHistory.goBack();
  };

  handleChange = event => {
    this.setState({ email: event.target.value });
  };

  handleSubmit = async event => {
    event.preventDefault();
    await this.props.checkEmail(this.state.email);
    const { route, email } = this.props;
    console.log('email', email);
    if (route === 'new_user') {
      return browserHistory.push('PersonalInfo');
    }
    if (route === 'authorized_email') {
      return browserHistory.push('Password');
    }
    return null;
  };

  render() {
    console.log('parent', this.props.parent);
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container ">
            <div className="row">
              <div className="col-md-3 col-lg-2 col-sm-12 child_vertical">
                <h3 className="widget-title">Edit profile</h3>
                <ul className="list-link">
                  <li>
                    <a href="" onClick={() => browserHistory.push('EditProfile')}>
                      General
                    </a>
                  </li>
                  <li>
                    <a href="" onClick={() => browserHistory.push('Shipping')}>
                      Shipping
                    </a>
                  </li>
                  <li className="active">
                    <a href="">Password</a>
                  </li>
                </ul>
              </div>
              <div className="pass-container-1  col-sm-12 child_vertical">
                <div className="form-line">
                    <label>Current password</label>
                    <div className="wrap-input wrap-input-password">
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                    </div>
                    <label>New password</label>
                    <div className="wrap-input wrap-input-password">
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                    </div>
                    <label>Repeat new password</label>
                    <div className="wrap-input wrap-input-password">
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                      <input
                        type="password"
                        inputMode="numeric"
                        pattern="[0-9]*"
                        maxLength={1}
                        size={1}
                        name="pass_zip"
                      />
                    </div>
                    <div className="form_group">
                      <button className="btn_grad">Save</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { parent } = user;
  return { parent };
};

export default connect(mapStateToProps, { checkEmail })(ChangePassword);
