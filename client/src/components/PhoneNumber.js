import React, { Component } from 'react';
import {browserHistory} from 'react-router';
import Header from './layouts/Header';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {updateChildData} from '../actions/index';
import {geolocationOption} from '../constants';
import {oneSignalService} from '../services/sendPN';
import ReactPhoneInput from 'react-phone-input-2';
import {register_socket} from '../actions/index';
import {registerSocket} from '../services/registerSocket';

class PhoneNumber extends Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            phoneNumber: '',
        };
    }

    handleOnChange(value) {
        this.setState({ phoneNumber: value });
    }
    componentWillMount(){
        registerSocket(this)
    }
    handleSubmit(e){
        this.props.updateChildData({
            phoneNumber: this.state.phoneNumber
        })
        navigator.geolocation.getCurrentPosition((position)=>{
            const lat = parseFloat(position.coords.latitude);
            const long = parseFloat(position.coords.longitude);
            const webInitLocation = {
                latitude: lat,
                longitude: long
            };
            this.props.updateChildData({
                webInitLocation: webInitLocation
            })
            browserHistory.push('/Connection');
            oneSignalService.sendPN([this.props.parentData.pushNotificationID], this.props.parentData.childName).then(resp=>{
                console.log('push is sent',resp.data);
            }).catch(err=>{
                console.warn('push has err',err);
            })
        }, (err)=>{
            alert("Location detection issue!\nCheck your location allow!")
            console.warn(err);
        }, geolocationOption);
    }
    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container text-center">
                        <h4>Phone number</h4>
                        <p>
                            Please enter your phone number.<br />
                            The number will be sent to the guardian.
                        </p>
                        <div className="phone_input_group">
                            <div className="form-line">
                                <div className="wrap-input">
                                    <ReactPhoneInput
                                        defaultCountry={'no'}
                                        onChange={this.handleOnChange.bind(this)}
                                        inputStyle={{
                                            border: 'none',
                                            width: 'inherit',
                                            paddingRight: 30
                                        }}
                                        buttonStyle={{
                                            border: 'none',
                                            background: 'transparent'
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="navigation text-center">
                            {
                                this.state.phoneNumber?
                                <a onClick={this.handleSubmit.bind(this)}>
                                    Next <i className="fa fa-angle-right" aria-hidden="true" />
                                </a>
                                :null
                            }
                        </div>
                    </div>
                </main>
                <img alt="logo" src="images/step1.jpg" className="step_img" style={{width: '25%'}} />
            </div>
        );
    }
}
const mapStateToProps = (state)=>{
    return {
        socket: state.socketData.socket,
        wristband_code: state.childData.wristband_code,
        parentData: state.parentData,
    }
}
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        updateChildData: updateChildData,
        register_socket: register_socket
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PhoneNumber);
