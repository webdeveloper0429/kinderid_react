import React, { Component } from 'react';
import Header from './layouts/Header';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import GoogleMapReact from 'google-map-react';
// import Geocode from "react-geocode";
// Geocode.setApiKey("AIzaSyAX9nzgzV4RBP8IiTtca6DdYN541EjM0T0");

const ChildMarker = () => {
  return(
    <div className="marker red">
      <div className="pin"></div>
      <div className="pin-effect"></div>
    </div>
  )
}
const ParentMarker = () => {
  return(
    <div className="marker blue">
      <div className="pin"></div>
      <div className="pin-effect"></div>
    </div>
  )
}

class FinderMap extends Component {
  constructor() {
    super();
    this.state = {
      map: null,
      maps: null,
      mobileAddress: '',
      defaultCenter: {
        lat: 0,
        lng: 0,
      },
      mobilePosition: {
        lat: 0,
        lng: 0,
      },
      webPosition: {
        lat: 0,
        lng: 0,
      },
    };
  }
  watchID = null;
  componentWillMount() {
    this.setState({
      defaultCenter: this.props.mobileInitLocation,
      mobilePosition: this.props.mobileInitLocation,
      webPosition: {
        lat: this.props.webInitLocation.latitude,
        lng: this.props.webInitLocation.longitude
      }
    });
  }
  componentDidMount() {
    this.registerConnection();    
    setTimeout(()=>{
      this.autoZoom();
      this.props.socket.emit(
        'webUpdated',
        this.props.mobile_socket_id,
        { latitude: this.state.webPosition.lat, longitude: this.state.webPosition.lng },
        this.props.phoneNumber
      );
    }, 3000)
    this.watchID = navigator.geolocation.watchPosition(position => {
      const lat = parseFloat(position.coords.latitude);
      const lng = parseFloat(position.coords.longitude);
      const lastRegion = {
        lat,
        lng,
      };
      this.setState({
        webPosition: lastRegion,
      });
      this.props.socket.emit(
        'webUpdated',
        this.props.mobile_socket_id,
        { latitude: lat, longitude: lng },
        this.props.phoneNumber
      );
    });
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }
  registerConnection() {
    this.props.socket.on('locationUpdateFromMobile', mobilePosition => {
      this.setState({
        mobilePosition,
      });
      if (this.state.maps) {
        const geocoder = new this.state.maps.Geocoder();
        geocoder.geocode({ location: mobilePosition }, (results, status) => {
          if (status === 'OK') {
            this.setState({ mobileAddress: results[0].formatted_address });
          }
        });
      }
    });
    this.props.socket.on('reunited_success', () => {
      browserHistory.push('/ThankYou');
    });
    this.props.socket.on('reunited_failed', () => {
      browserHistory.push('/ThankYou');
    });
  }
  autoZoom = () => {
    if (this.state.map) {
      const map = this.state.map;
      const maps = this.state.maps;
      const bounds = new maps.LatLngBounds();
      bounds.extend(new maps.LatLng(this.state.mobilePosition.lat, this.state.mobilePosition.lng));
      bounds.extend(new maps.LatLng(this.state.webPosition.lat, this.state.webPosition.lng));
      map.fitBounds(bounds);
    }
  };

  render() {
    return (
      <div id="wrapper" className="finderMap_screen">
        <Header />
        <main>
          <div className="container ">
            <div className="row">
              <div className="col-12 col-md-6 map_left">
                <h4>You’re now helping {this.props.parentData.childName}</h4>
                <p>Please contact Guardian to arrange a meeeting</p>
                <p>&nbsp;</p>
                    <div>Guardians name</div>
                    <h4>{this.props.parentData.parentName}</h4>
                    <div className="mobileNumWrapper">
                      <button className="btn_grad">{this.props.parentData.parentMobile}</button>
                    </div>
                    <div className="addressWrapper">
                      {this.state.mobileAddress}
                    </div>
              </div>
              <div className="col-12 col-md-6">
                <div className="map_wrapper" >
                  <div className="autozoom_w" onClick={this.autoZoom.bind(this)}>
                    <img alt="#" src="images/cursor.png" className="cursor_icon" />
                  </div>
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: ['AIzaSyDJcWRkEzgEVgkeQfP7oHMOc1zvvSuQVmk'] }}
                    defaultCenter={this.state.defaultCenter}
                    defaultZoom={5}
                    yesIWantToUseGoogleMapApiInternals
                    onGoogleApiLoaded={({ map, maps }) =>
                      this.setState({
                        map,
                        maps,
                      })
                    }
                  >
                    <ParentMarker lat={this.state.mobilePosition.lat} lng={this.state.mobilePosition.lng} />
                    <ChildMarker lat={this.state.webPosition.lat} lng={this.state.webPosition.lng} />
                  </GoogleMapReact>
                </div>
              </div>
            </div>
          </div>
        </main>
        <img alt="logo" src="images/step3.jpg" className="step_img" style={{width: '75%'}} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  socket: state.socketData.socket,
  web_socket_id: state.socketData.web_socket_id,
  mobile_socket_id: state.socketData.mobile_socket_id,
  mobileInitLocation: state.socketData.mobileInitLocation,
  wristband_code: state.childData.wristband_code,
  webInitLocation: state.childData.webInitLocation,
  parentData: state.parentData,
  phoneNumber: state.childData.phoneNumber,
});

export default connect(mapStateToProps)(FinderMap);
