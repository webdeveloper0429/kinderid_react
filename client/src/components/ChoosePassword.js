import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Header from './layouts/Header';
import { connect } from 'react-redux';
import { registerParent } from '../actions/authActions';

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pass1: '',
      pass2: '',
      pass3: '',
      pass4: '',
      pass5: '',
      pass6: '',
      rPass1: '',
      rPass2: '',
      rPass3: '',
      rPass4: '',
      rPass5: '',
      rPass6: '',
    };
  }

  componentDidMount(){
    this.pass1.focus();
  }

  onChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
    switch (name) {
      case 'pass1':
        this.pass2.focus();
        break;
      case 'pass2':
        this.pass3.focus();
        break;
      case 'pass3':
        this.pass4.focus();
        break;
      case 'pass4':
        this.pass5.focus();
        break;
      case 'pass5':
        this.pass6.focus();
        break;
      case 'rPass1':
        this.rPass2.focus();
        break;
      case 'rPass2':
        this.rPass3.focus();
        break;
      case 'rPass3':
        this.rPass4.focus();
        break;
      case 'rPass4':
        this.rPass5.focus();
        break;
      case 'rPass5':
        this.rPass6.focus();
        break;
      default:
        break;
    }
  };
  handleSubmit = async event => {
    event.preventDefault();
    const password = this.handlePassword();
    if (password === false) {
      return console.log('Password does not match');
    }
    const { email, fullName, mobile, address, post, city } = this.props;
    const user = { email, fullName, mobile, address, post, city };
    user.password = password;
    console.log('user ', user);

    await this.props.registerParent(user);
    console.log('token ', this.props.accessToken);
    localStorage.setItem('kinderIdAccessToken', JSON.stringify(this.props.accessToken));
    if (this.props.shoppingRoute === 'shopping') {
      return browserHistory.push('Summary');
    }
    return browserHistory.push('MainPage');
  };

  handlePassword = () => {
    const { pass1, pass2, pass3, pass4, pass5, pass6, rPass1, rPass2, rPass3, rPass4, rPass5, rPass6 } = this.state;
    const password = pass1 + pass2 + pass3 + pass4 + pass5 + pass6;
    const rPassword = rPass1 + rPass2 + rPass3 + rPass4 + rPass5 + rPass6;
    if (password !== rPassword || password === '') {
      return false;
    }
    return password;
  };

  goBack = () => {
    browserHistory.goBack();
  };
  render() {
    const valid = this.state.pass1 && this.state.pass2
                  && this.state.pass3 && this.state.pass4
                  && this.state.pass5 && this.state.pass6
                  && this.state.rPass1 && this.state.rPass2
                  && this.state.rPass3 && this.state.rPass4
                  && this.state.rPass5 && this.state.rPass6
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Choose password</h4>
            <p>Choose a 6 digit password</p>
            <div className="select-group select-group2" style={{ marginTop: 0 }}>
              <div className="form-line">
                <form>
                  <div className="wrap-input wrap-input-password">
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      value={this.state.pass1}
                      name="pass1"
                      ref={input => {
                        this.pass1 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass2"
                      value={this.state.pass2}
                      ref={input => {
                        this.pass2 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass3"
                      value={this.state.pass3}
                      ref={input => {
                        this.pass3 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass4"
                      value={this.state.pass4}
                      ref={input => {
                        this.pass4 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass5"
                      value={this.state.pass5}
                      ref={input => {
                        this.pass5 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass6"
                      value={this.state.pass6}
                      ref={input => {
                        this.pass6 = input;
                      }}
                      onChange={this.onChange}
                    />
                  </div>
                  <p>Repeat password</p>
                  <div className="wrap-input wrap-input-password">
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass1"
                      value={this.state.rPass1}
                      ref={input => {
                        this.rPass1 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass2"
                      value={this.state.rPass2}
                      ref={input => {
                        this.rPass2 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass3"
                      value={this.state.rPass3}
                      ref={input => {
                        this.rPass3 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass4"
                      value={this.state.rPass4}
                      ref={input => {
                        this.rPass4 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass5"
                      value={this.state.rPass5}
                      ref={input => {
                        this.rPass5 = input;
                      }}
                      onChange={this.onChange}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="rPass6"
                      value={this.state.rPass6}
                      ref={input => {
                        this.rPass6 = input;
                      }}
                      onChange={this.onChange}
                    />
                  </div>
                </form>
              </div>
            </div>
            <div className="navigation text-center">
              <a onClick={this.goBack}>
                <i className="fa fa-angle-left" aria-hidden="true" /> Back
              </a>
              {
                valid?
                <a onClick={this.handleSubmit}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
                :null
              }              
            </div>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { email, fullName, mobile, address, post, city, accessToken, shoppingRoute } = auth;
  return {
    email,
    fullName,
    mobile,
    address,
    post,
    city,
    accessToken,
    shoppingRoute,
  };
};

export default connect(mapStateToProps, { registerParent })(PersonalInfo);
