import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateCart } from '../actions/userActions';

class MainPage extends Component {
  constructor() {
    super();
    this.state = {
      red: 0,
      blue: 0,
      grey: 0,
      black: 0,
    };
  }

  goBack = () => {
    browserHistory.goBack();
  };

  increamentQuantity = color => {
    let quantity = this.state[color];
    quantity += 1;
    this.setState({ [color]: quantity });
  };

  decreamentQuantity = color => {
    let quantity = this.state[color];
    quantity -= 1;
    if (quantity <= 0) {
      quantity = 0;
    }
    this.setState({ [color]: quantity });
  };
  handleCart = () => {
    this.props.updateCart(this.state);
    return browserHistory.push('Subscription');
  };
  render() {
    const sum = this.state.red + this.state.blue + this.state.grey + this.state.black 
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Choose your color</h4>
            <p>
              Please select a colour, to add press choose<br />
              One size - 170mm x 12mm x 3mm<br />
              Fits age 3 to 11 years
            </p>
            <div className="row product-row">
              <div className="col-12 col-md-3 product-item">
                <div className="product-in">
                  <img alt="#" src="images/product1.png" />
                  <a onClick={() => this.increamentQuantity('red')}>
                    <div className="select-box select-box2">
                      <div className="icon">{this.state.red}</div>
                      <span>Choose</span>
                    </div>
                  </a>
                  <a href="" className="delete-link" onClick={() => this.decreamentQuantity('red')}>
                    Remove
                  </a>
                </div>
              </div>
              <div className="col-12 col-md-3 product-item">
                <div className="product-in">
                  <img alt="#" src="images/product2.png" />
                  <a onClick={() => this.increamentQuantity('blue')}>
                    <div className="select-box select-box2">
                      <div className="icon icon-blue">{this.state.blue}</div>
                      <span>Choose</span>
                    </div>
                  </a>
                  <a href="" className="delete-link" onClick={() => this.decreamentQuantity('blue')}>
                    Remove
                  </a>
                </div>
              </div>
              <div className="col-12 col-md-3 product-item">
                <div className="product-in">
                  <img alt="#" src="images/product3.png" />
                  <a onClick={() => this.increamentQuantity('grey')}>
                    <div className="select-box select-box2">
                      <div className="icon icon-gray">{this.state.grey}</div>
                      <span>Choose</span>
                    </div>
                  </a>
                  <a href="" className="delete-link" onClick={() => this.decreamentQuantity('grey')}>
                    Remove
                  </a>
                </div>
              </div>
              <div className="col-12 col-md-3 product-item">
                <div className="product-in">
                  <img alt="#" src="images/product4.png" />
                  <a onClick={() => this.increamentQuantity('black')}>
                    <div className="select-box select-box2">
                      <div className="icon icon-black">{this.state.black}</div>
                      <span>Choose</span>
                    </div>
                  </a>
                  <a href="" className="delete-link" onClick={() => this.decreamentQuantity('black')}>
                    Remove
                  </a>
                </div>
              </div>
            </div>
            <div className="navigation text-center">
              {
                sum>0?
                <a onClick={this.handleCart}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
                : null
              }
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default connect(null, { updateCart })(MainPage);
