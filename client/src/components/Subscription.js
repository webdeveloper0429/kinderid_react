import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateSub } from '../actions/userActions';
import { setRoute } from '../actions/authActions';
import _ from 'lodash';

class MainPage extends Component {
  constructor() {
    super();
    this.state = { 
      subscription: '',
      checked: true,
    };
  }

  goBack = () => {
    browserHistory.goBack();
  };

  handleCheck(){
    this.setState({ checked: !this.state.checked });
  }

  handleSubmit = () => {
    this.props.updateSub(this.state.subscription);
    if (_.isEmpty(this.props.parent)) {
      this.props.setRoute('shopping');
      return browserHistory.push('Signin');
    }
    return browserHistory.push('Summary');
  };

  render() {
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Choose subscription</h4>
            <p>
              The wristbands are free, you only pay for the subscription.<br />
              Please choose a subscription that suits your needs.<br />
              You need one subscription per wristband.
            </p>
            <div className="select-group select-group2">
              <div className="row">
                <div className="col-lg-6 col-12 ">
                  <a onClick={() => this.setState({ subscription: 'oneYear' })}>
                    <div className={"select-box "+(this.state.subscription === 'oneYear' ? "selected_sub": "")}>1 year 249 NOK</div>
                  </a>
                </div>
                <div className="col-lg-6 col-12 ">
                  <a onClick={() => this.setState({ subscription: 'threeYears' })}>
                    <div className={"select-box "+(this.state.subscription === 'threeYears' ? "selected_sub": "")}>
                      <div className="icon icon-blue">-33%</div>
                      3 year 499 NOK
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div className="gifimg">
              <div className="checkbox checkbox-warning">
                <div className="gift_check_wrapper"  onClick={this.handleCheck.bind(this)}>
                  <div className="cus_checkbox">
                    {
                      this.state.checked?
                      <div className="cus_checked">
                      </div>
                      :null
                    }
                  </div>
                  &nbsp;&nbsp;&nbsp;This is a gift
                </div>
              </div>
            </div>
            <div className="navigation text-center">
              <a onClick={this.goBack}>
                <i className="fa fa-angle-left" aria-hidden="true" /> Back
              </a>
              {
                this.state.subscription?
                <a onClick={this.handleSubmit}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
                :null
              }
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { parent, cart } = user;
  return { parent, cart };
};
export default connect(mapStateToProps, { setRoute, updateSub })(MainPage);
