import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateCart } from '../actions/userActions';
import Modal from 'react-bootstrap4-modal';

class MainPage extends Component {
    constructor(props){
        super(props);
        this.state={
            card: "",
            expiry: "",
            cvc: "",
            showModal: false,
            errorMessage: '',
            loading: false, disableButton: false,
        }
    }
    goBack = () => {
        browserHistory.goBack();
    };
    closeModal = () => {
        this.setState({ showModal: false, loading: false, disableButton: true });
      };
    onPay(e){
        e.preventDefault();
        this.setState({ disableButton: true, loading: true, showModal:true, errorMessage: "hi this is a test" });
        // browserHistory.push('/ThanksOrder');
    }
    renderSpinner() {
        if (this.state.loading) {
          return <i className="fa fa-spinner fa-spin" style={{ fontSize: '20px', fontWeight: 800 }} />;
        }
        return (
            <div>
        <button className="red-button d-none  d-md-block" onClick={this.onPay.bind(this)} >Pay {this.props.total} NOK</button>
        <button className="red-button d-block  d-md-none fixed-button" onClick={this.onPay.bind(this)} >Pay {this.props.total} NOK</button>
        </div>
    );
      }
    handleCard = card => {
        const v = card.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
        const matches = v.match(/\d{4,16}/g);
        const match = (matches && matches[0]) || "";
        const parts = [];
        for (let i = 0, len = match.length; i < len; i += 4) {
            parts.push(match.substring(i, i + 4));
        }
        if (parts.length) {
            this.setState({ card: parts.join(" ") });
        } else {
            this.setState({ card });
        }
    };
    handleExpiry = expiry => {
        const v = expiry.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
        const matches = v.match(/\d{2,5}/g);
        const match = (matches && matches[0]) || "";
        const parts = [];
        for (let i = 0, len = match.length; i < len; i += 2) {
            parts.push(match.substring(i, i + 2));
        }
        if (parts.length) {
            this.setState({ expiry: parts.join("/") });
        } else {
            this.setState({ expiry });
        }
    };
    handleCVC = cvc =>{
        this.setState({ cvc: cvc });
    }
    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container">
                        <div className="select-group select-group2 select-group3">
                            <div className="form-line center_text_form">
                                <form>
                                    <div className="wrap-input card-number-container">
                                        <input
                                            type="text"
                                            name="card"
                                            placeholder="Card number"
                                            maxLength={19}
                                            value={this.state.card}
                                            onChange={(e)=>this.handleCard(e.target.value.replace(/\D/, ''))}
                                        />
                                    </div>
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="wrap-input cc-bottom-input-container">
                                                <input
                                                    type="text"
                                                    name="expiry"
                                                    placeholder="Expriation"
                                                    maxLength={5}
                                                    value={this.state.expiry}
                                                    onChange={(e)=>this.handleExpiry(e.target.value.replace(/\D/, ''))}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="wrap-input cc-bottom-input-container">
                                                <input
                                                    type="text"
                                                    name="cvc"
                                                    placeholder="CVC"
                                                    maxLength={3}
                                                    value={this.state.cvc}
                                                    onChange={(e)=>this.handleCVC(e.target.value.replace(/\D/, ''))}
                                                />
                                            </div>
                                        </div>                                        
                                    </div>
                                    {this.renderSpinner()}

                                    <div className="text-center">
                                        <img className="stripe" src="images/stripe.png" alt="#" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <div className="checkout_modal">
                    <Modal visible={this.state.showModal} >
                        <div className="">
                            <div className="modal_head" >
                                CardInvalid
                            </div>
                            <div className="err_text">
                                <p>{this.state.errorMessage}</p>
                            </div>
                            <div className="modal_btn" onClick={this.closeModal.bind(this)}>
                                OK
                            </div>
                        </div>
                    </Modal>
                </div>
            </div>
        );
    }
}
const mapStateToProps = ({ user }) => {
    const { total } = user;
    return { total };
};

export default connect(mapStateToProps, { updateCart })(MainPage);
