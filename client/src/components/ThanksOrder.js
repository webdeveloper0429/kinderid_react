import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateCart } from '../actions/userActions';

class ThanksOrder extends Component {
  goBack = () => {
    browserHistory.push('/');
  };
  render() {
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-left">
            <div className="select-group select-group2 select-group3">
              <div className="form-line">
                <div>
                  <div className="shadow_board">
                    <div className="title">
                      Thank you for your order!
                    </div>
                    <div className="order_id">
                      order ID: {"123456"}
                    </div>
                    <div className="price">
                      {this.props.total} NOK
                    </div>
                    <div className="vat">
                      (25% VAT: 155NOK)
                    </div>
                    <div className="card_info">
                      <div className="">
                        Payment card
                      </div>
                      <div className="user_card">
                        VISA x.xx16
                      </div>
                    </div>
                  </div>
                  <button className="red-button d-none  d-md-block" onClick={this.goBack.bind(this)} >Go back</button>
                  <button className="red-button d-block  d-md-none fixed-button" onClick={this.goBack.bind(this)} >Go back</button>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { total } = user;
  return { total };
};

export default connect(mapStateToProps, { updateCart })(ThanksOrder);
