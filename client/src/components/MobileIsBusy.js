import React, { Component } from 'react';
import Header from './layouts/Header';
import { Link } from 'react-router';

class MobileIsBusy extends Component {
  render() {
    return (
      <div id="wrapper">
        <Header />
        <div className="fail_div">
          <img alt="" src="images/failed.png" width="50px" />
          <span style={{ margin: 20 }}>Parent is busy</span>
          <Link to="/">
            <button type="button" className="btn btn-primary mb-2">
              Try Again
            </button>
          </Link>
        </div>
      </div>
    );
  }
}

export default MobileIsBusy;
