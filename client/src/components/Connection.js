import React, { Component } from 'react';
import Header from './layouts/Header';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {updateChildData, register_socket} from '../actions/index';
import {oneSignalService} from '../services/sendPN';

class Connecting extends Component {
    constructor() {
        super();
        this.state = {
            parentPing: true,
            guardianPing: true,
        };
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({ parentPing: false });
            if(this.props.parentData.gaurdians){
                this.props.parentData.gaurdians.map((guardian)=>{
                    this.props.socket.on("failedGuardian_"+guardian.gaurdianRefId, ()=>{
                        console.log(`*${guardian.gaurdianRefId}* guardian connection is failed!`);
                        if(this.state.guardianPing){
                            setTimeout(()=>{
                                this.searchGuardin(guardian.gaurdianRefId);
                            }, 1000);
                        }
                    });
                    if(this.state.guardianPing){
                        this.searchGuardin(guardian.gaurdianRefId);
                        if(guardian._gaurdianPushNotificationID){
                            oneSignalService.sendPN([guardian._gaurdianPushNotificationID], this.props.parentData.childName).then(resp=>{
                                console.log('push is sent',resp.data);
                            }).catch(err=>{
                                console.warn('push has err',err);
                            })
                        }
                    }
                    return true;
                })
            }
        }, 60000);
        this.props.socket.on("acceptedConnection", (web_socket_id, mobile_socket_id, mobileInitLocation, user_id)=>{
            console.log(`mobile accepted connection`);
            this.setState({
                parentPing: false,
                guardianPing: false
            })
            ///guarndian is looking for child
            if(this.props.parentData.parentId !== user_id){
                this.props.socket.emit("guardianConnectedParent", this.props.wristband_code);
            }
            if(this.props.parentData.gaurdians){
                this.props.parentData.gaurdians.map((guardian)=>{
                    if(guardian.gaurdianRefId !== user_id){
                        this.props.socket.emit("guardianConnectedGuardian", guardian.gaurdianRefId);
                    }
                    return true;
                })
            }
            this.props.register_socket({
                web_socket_id: web_socket_id,
                mobile_socket_id: mobile_socket_id,
                mobileInitLocation: mobileInitLocation
            });
            browserHistory.push('/FinderMap');
        });
        this.props.socket.on("mobileIsBusy", (wristband_code)=>{
            console.log(`*${wristband_code}* mobile is busy`);
            browserHistory.push('/MobileIsBusy');
        });
        this.props.socket.on("declinedConnection", ()=>{
            console.log("Parent declined connection!")
            browserHistory.push('/FailedConnection');
        })
        this.props.socket.on("failedConnection", (wristband_code)=>{
            console.log(`*${wristband_code}* connection failed`);
            if(this.state.parentPing){
                setTimeout(()=>{
                    this.searchMobile();
                }, 1000);
            }
        });
        this.searchMobile()
    }
    searchMobile(){
        this.props.socket.emit("searchMobile", this.props.wristband_code, this.props.webInitLocation);
    }
    searchGuardin(gaurdianRefId){
        const childData={
            wristband_code: this.props.wristband_code,
            childName: this.props.parentData.childName,
            parentName: this.props.parentData.parentName
        }
        this.props.socket.emit("searchGuardian", gaurdianRefId, childData, this.props.webInitLocation);
    }
    render() {
        return (
            <div id="wrapper">
                <Header />
                <div className="loading_icon" />
                <img alt="logo" src="images/step2.jpg" className="step_img" style={{width: '50%'}} />
            </div>
        );
    }
}
const mapStateToProps = (state)=>{
    return {
        socket: state.socketData.socket,
        wristband_code: state.childData.wristband_code,
        webInitLocation: state.childData.webInitLocation,
        parentData: state.parentData
    }
}
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        updateChildData: updateChildData,
        register_socket: register_socket
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Connecting);