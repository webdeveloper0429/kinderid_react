import React, { Component } from 'react';
import { Link } from 'react-router';

class Header extends Component {
    componentDidMount(){
        window.$("#mobilemenu").mmenu({
            extensions 	: [ "shadow-panels", "fx-panels-slide-100", "border-none", "theme-black", "fullscreen" ],
            navbars		: {
                title : "",
                add: false,
                content : [ "<div class='row'><div class='col-5'><img src='images/logo-white.png' /></div></div>", "close" ],
                height 	: 2
            },
            setSelected: false,
        }, { });
    }
    render() {
        const localToken = localStorage.getItem('kinderIdAccessToken');
        return (
            <header id="header">
                <div className="container">
                    <div className="row align-items-center">
                        <h1 id="logo" className="col-5">
                            <a href="/">
                                {
                                    this.props.whiteLogo?
                                    <img alt="logo" src="images/logo-w.svg" />
                                    :
                                    <img alt="logo" src="images/logo-b.svg" />
                                }
                            </a>
                        </h1>
                        <div className="col-7 ml-auto">
                            <nav id="mobilemenu" className="d-none d-md-none">
                                <ul id="panel-menu">
                                    <li>
                                        <a href="/">Home</a>
                                    </li>
                                    <li>
                                        <a href="/Shop">Shop</a>
                                    </li>
                                    <li>
                                        <a href="/HaveAccount">Support</a>
                                    </li>
                                    <li>
                                        <a href="/FAQ">FAQ</a>
                                    </li>
                                    <li>
                                        <a href="/Legal">Legal</a>
                                    </li>
                                </ul>
                            </nav>
                            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                                <a href="#mobilemenu" className="navbar-toggler ">
                                    <span className="navbar-toggler-icon"></span>
                                </a>
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul className="navbar-nav ml-auto">
                                        <li className="nav-item">
                                            <Link className="nav-link" to="/Shop">
                                                Shop
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            {
                                                localToken?
                                                <Link className="nav-link has-border" to="/MainPage">
                                                    <strong>Profile</strong>
                                                </Link>
                                                :
                                                <Link className="nav-link has-border" to="/Signin">
                                                    <strong>Sign in</strong>
                                                </Link>
                                            }
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;
