import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';

class Contact extends Component {
    constructor() {
        super();
        this.state = {
            contactText: '',
            kinderId: ''
        };
    }

    goBack = () => {
        browserHistory.goBack();
    };

    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container text-center">
                        <h4>What can we<br/>help you with?</h4>
                        <div className="contact_container">
                            <textarea
                                placeholder="Start typing"
                                className="contact_textarea"
                                value={this.state.contactText}
                                onChange={e=>this.setState({ contactText: e.target.value })}
                            />
                            <div className="contact_upload">
                                <i className="fa fa-upload" aria-hidden="true"></i>
                                &nbsp;&nbsp;Upload file
                            </div>
                            <div className="form-line select-group4">
                                <span>Type in the relevant KinderID</span>
                                <div className="wrap-input ">
                                    <input
                                        type="text"
                                        value={this.state.kinderId}
                                        onChange={e=>this.setState({ kinderId: e.target.value })}
                                        placeholder="AB 000000"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="navigation text-center">
                            <a onClick={this.goBack}>
                                <i className="fa fa-angle-left" aria-hidden="true" /> Back
                            </a>
                            <a onClick={this.handleCart}>
                                Next <i className="fa fa-angle-right" aria-hidden="true" />
                            </a>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default connect(null, null)(Contact);
