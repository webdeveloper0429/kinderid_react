import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateChildData, updateParentData } from '../actions/index';
import { ParentService } from '../services/parentService';

import Header from './layouts/Header';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.onWristbandChange = this.onWristbandChange.bind(this);
    this.state = {
      wristband_code: '',
    };
  }
  onWristbandChange(value) {
    let val = value.toUpperCase().replace(/\s/g, '');;
    if(val.length > 8) return false;
    let testStr = val + "AB000000".slice(val.length);
    const regex = /[A-Z]{2}[0-9]{6}/g;
    if(regex.test(testStr)){
      let output = val;
      if(val.length>2){
        output = [val.slice(0, 2), ' ', val.slice(2)].join('');
      }
      this.setState({ wristband_code: output });
    }
  }
  onClickNext() {
    if(!this.state.wristband_code) return false;
    this.props.updateChildData({
      wristband_code: this.state.wristband_code,
    });
    ParentService.getData(this.state.wristband_code).then(resp => {
      this.props.updateParentData(resp.data);
    })
    .catch(err=>{
      console.warn('getting parent data is failed',err)
    });
    browserHistory.push('/PhoneNumber');
  }
  render() {
    const localToken = localStorage.getItem('kinderIdAccessToken');
    return (
      <div id="wrapper">
        <div id="wrapper-top" className="bg-red">
          <Header whiteLogo={true} />
          <main>
            <div className="container ">
              <div className="sub-container">
                <div className="row align-items-center">
                  <div className="col-12 col-sm-12 col-lg-6 ">
                    <h1 className="homepage-main-title">
                      Have you<br />found a child?
                    </h1>
                    <p className="small-font">Please enter the ID from the childs wristband</p>

                    <div className="form-group clearfix">
                      <input
                        type="text"
                        className="form-control mini-input input-code float-left"
                        placeholder="AB 000000"
                        value={this.state.wristband_code}
                        onChange={(e)=>this.onWristbandChange(e.target.value)}
                      />
                      <button className="btn btn-primary" onClick={this.onClickNext.bind(this)}>
                        <i className="fa fa-angle-right d-block d-sm-block d-md-block d-lg-none" aria-hidden="true" />
                        <span className="d-none d-lg-block">Next</span>
                      </button>
                    </div>

                    <div className="play-box clearfix">
                      <a href="">
                        <span className="d-block d-lg-inline-block ">
                          <img width={60} alt="" className="d-none d-lg-block" src="images/play-btn.svg" />
                          <i className="fa fa-play-circle d-block d-lg-none" aria-hidden="true" />
                        </span>{' '}
                        Watch tutorial
                      </a>
                    </div>
                    <div className="row app-row ">
                      <div className="col-6 d-block d-lg-none">
                        <a className="button white-button " href="">
                          <i className="fa fa-apple" /> APPLE STORE
                        </a>
                      </div>
                      <div className="col-6 d-block d-lg-none">
                        <a className="button white-button " href="">
                          <i className="fa fa-android" /> GOOGLE PLAY
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
          <img alt="" src="images/phone-hand.png" className="d-none d-lg-block fullscreenimg" />
        </div>
        <div className="section">
          <div className="container text-center">
            <h4 className="box-heading">How does it work?</h4>
            <div className="empty-box" />
          </div>
        </div>
        <div className="section grey-section text-center">
          <h4>Trusted by</h4>
          <div className="container">
            <img alt="" className="logo-list tusenfryd" src="images/tusenfryd.svg" />
            <img alt="" className="logo-list flyoget" src="images/flyoget.svg" />
            <img alt="" className="logo-list if" src="images/if.svg" />
          </div>
        </div>
        <div className="section">
          <img width="100%" alt="" src="images/video-img.jpg" />
        </div>
        <div className="section section-app d-none d-lg-block">
          <div className="container text-center">
            <div className="app-container">
              <h4>Download the KinderID app</h4>
              <p>KinderID is avalible on the App Store and on Google Play</p>
              <div className="row  ">
                <div className="col-6 ">
                  <a className="button" href="">
                    <i className="fa fa-apple" /> APPLE STORE
                  </a>
                </div>
                <div className="col-6 ">
                  <a className="button" href="">
                    <i className="fa fa-android" /> GOOGLE PLAY
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer id="footer">
          <div className="container">
            <div className="row align-items-end">
              <div className="col-5">
                <ul className="menu">
                  <li>
                    <Link to="/faq">Faq</Link>
                  </li>
                  <li>
                    {
                      localToken?
                      <Link to="/Contact">Customer service</Link>
                      :
                      <Link to="/HaveAccount">Customer service</Link>
                    }
                  </li>
                  <li>
                    <a href="">Business</a>
                  </li>
                  <li>
                    <a href="">Affiliates</a>
                  </li>
                </ul>
              </div>
              <div className="col-7 text-right">Safety Innovation AS © 2018</div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateChildData,
      updateParentData,
    },
    dispatch
  );
export default connect(null, mapDispatchToProps)(Landing);
