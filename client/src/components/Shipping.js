import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { fetchUserData } from '../actions/userActions';
import _ from 'lodash';

class Shipping extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  goBack = () => {
    browserHistory.goBack();
  };
  componentWillMount() {
    this.getUser();
  }
  getUser = async () => {
    await this.props.fetchUserData();
    return this.setState({ loading: false });
  };

  handleChange = event => {
    this.setState({ email: event.target.value });
  };

  render() {
    if (this.state.loading === true) {
      return <div />;
    }
    let address = '';
    let city = '';
    let post = '';
    if (_.some(this.props.parent, 'address')) {
      address = this.props.parent.address.address;
      city = this.props.parent.address.city;
      post = this.props.parent.address.post;
    }
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container ">
            <div className="row">
              <div className="col-md-3 col-lg-2 col-sm-12 child_vertical">
                <h3 className="widget-title">Edit profile</h3>
                <ul className="list-link">
                  <li>
                    <a href="" onClick={() => browserHistory.push('EditProfile')}>
                      General
                    </a>
                  </li>
                  <li className="active">
                    <a href="">Shipping</a>
                  </li>
                  <li>
                    <a href="" onClick={() => browserHistory.push('ChangePassword')}>
                      Password
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-4 col-lg-3 col-sm-12 child_vertical">
                <div className="form-line">
                    <label>Country</label>
                    <div className="wrap-input ">
                      <input type="text" name="name" value="Norway" />
                    </div>
                    <label>Address</label>
                    <div className="wrap-input ">
                      <input type="text" name="phone" value={address} />
                    </div>
                    <label>Postal code</label>
                    <div className="wrap-input">
                      <input type="email" name="name" value={post} />
                    </div>
                    <label>City</label>
                    <div className="wrap-input">
                      <input type="email" name="name" value={city} />
                    </div>
                    <div className="form_group">
                      <button className="btn_grad">Save</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { parent } = user;
  return { parent };
};

export default connect(mapStateToProps, { fetchUserData })(Shipping);
