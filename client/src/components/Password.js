import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Header from './layouts/Header';
import axios from 'axios';
import { connect } from 'react-redux';
import { registerParent } from '../actions/authActions';
import Modal from 'react-bootstrap4-modal';

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.state = {
      pass1: '',
      pass2: '',
      pass3: '',
      pass4: '',
      pass5: '',
      pass6: '',
      showModal: false,
      errorMessage: '',
    };
  }
  componentDidMount() {
    this.pass1.focus();
  }
  onKeyDown(e) {
    const { name } = e.target;
    if (e.keyCode === 8) {
      switch (name) {
        case 'pass1':
          break;
        case 'pass2':
          this.setState({ pass1: '' });
          this.pass1.focus();
          break;
        case 'pass3':
          this.setState({ pass2: '' });
          this.pass2.focus();
          break;
        case 'pass4':
          this.setState({ pass3: '' });
          this.pass3.focus();
          break;
        case 'pass5':
          this.setState({ pass4: '' });
          this.pass4.focus();
          break;
        case 'pass6':
          this.setState({ pass5: '' });
          this.setState({ pass6: '' });
          this.pass5.focus();
          break;
        default:
          break;
      }
    }
    return false;
  }
  onChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
    switch (name) {
      case 'pass1':
        this.pass2.focus();
        break;
      case 'pass2':
        this.pass3.focus();
        break;
      case 'pass3':
        this.pass4.focus();
        break;
      case 'pass4':
        this.pass5.focus();
        break;
      case 'pass5':
        this.pass6.focus();
        break;
      default:
        break;
    }
  };
  handleSubmit = async event => {
    event.preventDefault();
    const { pass1, pass2, pass3, pass4, pass5, pass6 } = this.state;
    const password = pass1 + pass2 + pass3 + pass4 + pass5 + pass6;

    if (password === '') {
      return console.log('Password does not match');
    }
    const { email } = this.props;
    const url = 'https://api.kinder-id.com/auth/mobile/authorize_user';
    try {
      const { data } = await axios.post(url, { email, password });
      console.log('respond from authorization', data.token);

      localStorage.setItem('kinderIdAccessToken', JSON.stringify(data.token));
      if (this.props.shoppingRoute === 'shopping') {
        return browserHistory.push('Summary');
      }
      return browserHistory.push('MainPage');
    } catch (err) {
      if (err.response.status === 401) {
        this.setState({
          showModal: true,
          errorMessage: 'Unauthorised',
        });
      }
    }
  };

  handlePassword = () => {
    const { pass1, pass2, pass3, pass4, pass5, pass6, rPass1, rPass2, rPass3, rPass4, rPass5, rPass6 } = this.state;
    const password = pass1 + pass2 + pass3 + pass4 + pass5 + pass6;
    const rPassword = rPass1 + rPass2 + rPass3 + rPass4 + rPass5 + rPass6;
    if (password !== rPassword || password === '') {
      return false;
    }
    return password;
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  goBack = () => {
    browserHistory.goBack();
  };
  render() {
    const allIsSet =
      this.state.pass1 &&
      this.state.pass2 &&
      this.state.pass3 &&
      this.state.pass4 &&
      this.state.pass5 &&
      this.state.pass6;
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container text-center">
            <h4>Password</h4>
            <p>Enter your 6-digit password</p>
            <div className="select-group select-group2" style={{ marginTop: 0 }}>
              <div className="form-line">
                <Modal visible={this.state.showModal} onClickBackdrop={this.modalBackdropClicked}>
                  <div className="modal-header">
                    <h5 className="modal-title">Error!</h5>
                  </div>
                  <div className="modal-body">
                    <p>{this.state.errorMessage}</p>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-primary" onClick={this.closeModal}>
                      OK
                    </button>
                  </div>
                </Modal>
                <form>
                  <div className="wrap-input wrap-input-password">
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      value={this.state.pass1}
                      name="pass1"
                      ref={input => {
                        this.pass1 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass2"
                      value={this.state.pass2}
                      ref={input => {
                        this.pass2 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass3"
                      value={this.state.pass3}
                      ref={input => {
                        this.pass3 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass4"
                      value={this.state.pass4}
                      ref={input => {
                        this.pass4 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass5"
                      value={this.state.pass5}
                      ref={input => {
                        this.pass5 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                    <input
                      type="password"
                      inputMode="numeric"
                      pattern="[0-9]*"
                      maxLength={1}
                      size={1}
                      name="pass6"
                      value={this.state.pass6}
                      ref={input => {
                        this.pass6 = input;
                      }}
                      onChange={this.onChange}
                      onKeyDown={this.onKeyDown}
                    />
                  </div>
                  <p>
                    <a>Forgot password?</a>
                  </p>
                </form>
              </div>
            </div>
            <div className="navigation text-center">
              <a onClick={this.goBack}>
                <i className="fa fa-angle-left" aria-hidden="true" /> Back
              </a>
              {allIsSet ? (
                <a onClick={this.handleSubmit}>
                  Next <i className="fa fa-angle-right" aria-hidden="true" />
                </a>
              ) : null}
            </div>
          </div>
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { email, shoppingRoute } = auth;
  return {
    email,
    shoppingRoute,
  };
};

export default connect(mapStateToProps, { registerParent })(PersonalInfo);
