import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { fetchUserData } from '../actions/userActions';
import ReactPhoneInput from 'react-phone-input-2';

class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  goBack = () => {
    browserHistory.goBack();
  };

  componentWillMount() {
    this.getUser();
    this.setState({ loading: false });
  }
  getUser = async () => {
    await this.props.fetchUserData();
    console.log('Artem this is your data', this.props.parent);
  };

  handleChange = event => {
    this.setState({ email: event.target.value });
  };

  render() {
    if (this.state.loading === true) {
      return <div />;
    }
    return (
      <div id="wrapper">
        <Header />
        <main>
          <div className="container ">
            <div className="row">
              <div className="col-md-3 col-lg-2 col-sm-12 child_vertical">
                <h3 className="widget-title">Edit profile</h3>
                <ul className="list-link">
                  <li className="active">
                    <a href="">General</a>
                  </li>
                  <li>
                    <a href="" onClick={() => browserHistory.push('Shipping')}>
                      Shipping
                    </a>
                  </li>
                  <li>
                    <a href="" onClick={() => browserHistory.push('ChangePassword')}>
                      Password
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-4 col-lg-3 col-sm-12 child_vertical">
                <div className="form-line">
                    <label>Full name</label>
                    <div className="wrap-input input-check">
                      <input type="text" name="name" value={this.props.parent.parentName} />
                    </div>
                    <label>Telephone number</label>
                    <div className="wrap-input input-error">
                      <ReactPhoneInput
                          defaultCountry={'no'}
                          value={this.props.parent.mobile}
                          inputStyle={{
                              border: 'none',
                              width: 'inherit',
                              paddingRight: 30,
                              fontSize: 18
                          }}
                          buttonStyle={{
                              border: 'none',
                              background: 'transparent'
                          }}
                      />
                    </div>
                    <label>Email address</label>
                    <div className="wrap-input">
                      <input type="email" name="name" value={this.props.parent.email} />
                    </div>
                    <div className="form_group">
                      <button className="btn_grad">Save</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
const mapStateToProps = ({ user }) => {
  const { parent } = user;
  return { parent };
};

export default connect(mapStateToProps, { fetchUserData })(EditProfile);
