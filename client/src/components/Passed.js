import React from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePasscode } from '../actions/index';
import sha1 from 'sha1';

class Passed extends React.Component{

    componentWillMount(){
        if(sha1(this.props.passcode) !== '2ffb22ff95ac122792bcf70c6ac53184ec1bb4cf'){
            this.props.updatePasscode('');
            browserHistory.push("/Passcode");
        }
    }

    render(){
        return(
            this.props.children
        )
    }
}

const mapStateToProps = (state)=>{
    return {
        passcode: state.passcode
    }
}

const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        updatePasscode: updatePasscode
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Passed);