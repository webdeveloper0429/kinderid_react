import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { updateTotal } from '../actions/userActions';
import { setRoute } from '../actions/authActions';
import { PRICE } from '../constants';

class MainPage extends Component {
    constructor(props){
        super(props);
        this.checkout = this.checkout.bind(this);
    }

    goBack = () => {
        browserHistory.goBack();
    };

    checkout (total) {
        console.log('toal', total);

        this.props.updateTotal(total);
        browserHistory.push('Checkout');
    };

    render() {
        console.log('sub', this.props.sub);
        console.log('cart', this.props.cart);
        if (this.props.sub === '') {
            return null;
        }
        const price = this.props.sub === 'oneYear' ? PRICE.ONE_YEAR : PRICE.THREE_YEAR;
        const {red, blue, grey, black} = this.props.cart;
        const total = (red + blue + grey + black) * price;
        let years = '';

        switch (this.props.sub) {
            case 'oneYear':
                years = '1 year';
                break;
            case 'threeYears':
                years = '3 years';
                break;
            default:
                break;
        }

        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container ">
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <div className="row align-items-center">
                                    <div className="col-6">
                                        <h4>Order summary</h4>
                                    </div>
                                    <div className="col-6 slale-col">Order is saved for 60 minutes</div>
                                </div>
                                <hr />
                                {
                                    this.props.cart.red > 0?
                                    <div className="row cart-item">
                                        <div className="col-4">
                                            <div className="item-img">
                                                <img alt="#" src="images/product1.png" width="100%" />
                                            </div>
                                        </div>
                                        <div className="col-8">
                                            <div className="product-name">{this.props.cart.red * price} NOK</div>
                                            {this.props.cart.red}x {years} subscription<br />
                                            {this.props.cart.red}x Red KinderID wristband
                                        </div>
                                    </div>
                                    :null
                                }
                                {
                                    this.props.cart.blue > 0?
                                    <div className="row cart-item">
                                        <div className="col-4">
                                            <div className="item-img">
                                                <img alt="#" src="images/product2.png" width="100%" />
                                            </div>
                                        </div>
                                        <div className="col-8">
                                            <div className="product-name">{this.props.cart.blue * price} NOK</div>
                                            {this.props.cart.blue}x {years} subscription<br />
                                            {this.props.cart.blue}x Blue KinderID wristband
                                        </div>
                                    </div>
                                    :null
                                }
                                {
                                    this.props.cart.grey > 0?
                                    <div className="row cart-item">
                                        <div className="col-4">
                                            <div className="item-img">
                                                <img alt="#" src="images/product3.png" width="100%" />
                                            </div>
                                        </div>
                                        <div className="col-8">
                                            <div className="product-name">{this.props.cart.grey * price} NOK</div>
                                            {this.props.cart.grey}x {years} subscription<br />
                                            {this.props.cart.grey}x Grey KinderID wristband
                                        </div>
                                    </div>
                                    :null
                                }
                                {
                                    this.props.cart.black > 0?
                                    <div className="row cart-item">
                                        <div className="col-4">
                                            <div className="item-img">
                                                <img alt="#" src="images/product4.png" width="100%" />
                                            </div>
                                        </div>
                                        <div className="col-8">
                                            <div className="product-name">{this.props.cart.black * price} NOK</div>
                                            {this.props.cart.black}x {years} subscription<br />
                                            {this.props.cart.black}x Black KinderID wristband
                                        </div>
                                    </div>
                                    :null
                                }
                            </div>
                            <div className="col-12 col-md-4 ml-auto">
                                <h4 style={{ paddingBottom: 8 }}>Total</h4>
                                <hr />
                                <div className="cart-item">
                                    <ul className="list-total">
                                        <li>
                                            <strong>Sub-total</strong> <span className="float-right"> {total} NOK</span>
                                        </li>
                                        <li>
                                            <strong>Delivery</strong> <span className="small">(international express freight)</span>{' '}
                                            <span className="float-right"> 29 NOK</span>
                                        </li>
                                    </ul>
                                </div>
                                <hr className="d-none d-md-block" />
                                <div className="cart-item d-none d-md-block">
                                    <button className="red-button" onClick={()=>this.checkout(total)}>Checkout</button>
                                </div>
                                <button className="red-button d-block  d-md-none fixed-button" onClick={()=>this.checkout(total)}>
                                    Checkout
                                </button>
                                <hr />
                                <p>
                                    <strong>We accept</strong>
                                </p>
                                <img alt="#" src="images/card-brands.png" width="100%" style={{marginBottom: 30}}/>
                                <hr />
                                <p>
                                    <strong>Do you have discount code?</strong>
                                </p>
                                <input className="summary_discount" 
                                    placeholder="000-000-000"
                                />
                                <hr />
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div className="navigation2 ">
                            <a onClick={this.goBack}>
                                <i className="fa fa-angle-left" aria-hidden="true" /> Back
                            </a>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}
const mapStateToProps = ({ user }) => {
    const { parent, cart, sub } = user;
    return { parent, cart, sub };
};
export default connect(mapStateToProps, { setRoute, updateTotal })(MainPage);
