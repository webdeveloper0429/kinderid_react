import React from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePasscode } from '../actions/index';

class Passcode extends React.Component{
    
    onClickNext(){
        browserHistory.push("/");
    }

    render(){
        return(
            <div id="wrapper">
                <div id="wrapper-top" className="bg-red">
                    <div style={{
                        height: '100vh',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }} >
                        
                        <h1 className="homepage-main-title">
                            Please enter passcode!
                        </h1>
                        <div className="form-group clearfix">
                            <input
                                type="password"
                                className="form-control mini-input input-code float-left"
                                placeholder=""
                                value={this.props.passcode}
                                onChange={(e)=>this.props.updatePasscode(e.target.value)}
                            />
                            <button className="btn btn-primary" onClick={this.onClickNext.bind(this)}>
                                <i className="fa fa-angle-right d-block d-sm-block d-md-block d-lg-none" aria-hidden="true" />
                                <span className="d-none d-lg-block">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return {
        passcode: state.passcode
    }
}

const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        updatePasscode: updatePasscode
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Passcode);