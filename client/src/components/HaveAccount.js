import React, { Component } from 'react';
import Header from './layouts/Header';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';

class HaveAccount extends Component {
    constructor() {
        super();
        this.state = {
        };
    }

    goBack = () => {
        browserHistory.goBack();
    };
    goSignin(){
        browserHistory.push('/signin');
    }

    render() {
        return (
            <div id="wrapper">
                <Header />
                <main>
                    <div className="container text-center">
                        <h4>Do you have an account?</h4>
                        <p>
                            Lorem ipsum dollor simet amet
                        </p>
                        <div className="have_account_container">
                            <div className="select-box " onClick={this.goSignin.bind(this)}>No</div>
                            <div className="select-box " onClick={this.goSignin.bind(this)}>Yes</div>
                        </div>
                        <div className="navigation text-center">
                            <a onClick={this.handleCart}>
                                Next <i className="fa fa-angle-right" aria-hidden="true" />
                            </a>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default connect(null, null)(HaveAccount);
