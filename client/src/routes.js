import React from 'react';
import { Route } from 'react-router';
// import Layout from './components/layouts'
import Landing from './components/Landing';
import Signin from './components/Signin';
import PersonalInfo from './components/PersonalInfo';
import PhoneNumber from './components/PhoneNumber';
import Connection from './components/Connection';
import FinderMap from './components/FinderMap';
import ThankYou from './components/ThankYou';
import FailedConnection from './components/FailedConnection';
// import NotFound from './components/NotFound';
import ChoosePassword from './components/ChoosePassword';
import MainPage from './components/MainPage';
import Password from './components/Password';
import EditProfile from './components/EditProfile';
import Shipping from './components/Shipping';
import ChangePassword from './components/ChangePassword';
import Shop from './components/Shop';
import Subscription from './components/Subscription';
import Checkout from './components/Checkout';
import Summary from './components/Summary';
import Legal from './components/Legal';
import FAQ from './components/FAQ';
import MobileIsBusy from './components/MobileIsBusy';
import ThanksOrder from './components/ThanksOrder';
import Passed from './components/Passed';
import Passcode from './components/Passcode';
import HaveAccount from './components/HaveAccount';
import Contact from './components/Contact';

export default store => (
  <Route>
    <Route component={Passed} >
      <Route path="/" component={Landing} />
      <Route path="/Signin" component={Signin} />
      <Route path="/PersonalInfo" component={PersonalInfo} />
      <Route path="/ChoosePassword" component={ChoosePassword} />
      <Route path="/PhoneNumber" component={PhoneNumber} />
      <Route path="/Connection" component={Connection} />
      <Route path="/FinderMap" component={FinderMap} />
      <Route path="/ThankYou" component={ThankYou} />
      <Route path="/MainPage" component={MainPage} />
      <Route path="/Password" component={Password} />
      <Route path="/EditProfile" component={EditProfile} />
      <Route path="/Shipping" component={Shipping} />
      <Route path="/ChangePassword" component={ChangePassword} />
      <Route path="/Shop" component={Shop} />
      <Route path="/Subscription" component={Subscription} />
      <Route path="/Summary" component={Summary} />
      <Route path="/FailedConnection" component={FailedConnection} />
      <Route path="/Checkout" component={Checkout} />
      <Route path="/Legal" component={Legal} />
      <Route path="/FAQ" component={FAQ} />
      <Route path="/MobileIsBusy" component={MobileIsBusy} />
      <Route path="/ThanksOrder" component={ThanksOrder} />
      <Route path="/HaveAccount" component={HaveAccount} />
      <Route path="/Contact" component={Contact} />
    </Route>    
    <Route path="/Passcode" component={Passcode} />
  </Route>
);
