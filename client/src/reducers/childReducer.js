const INITIAL_STATE = {
    wristband_code: '',
    phoneNumber: '',
    webInitLocation: {
        latitude: 0,
        longitude: 0
    }
};

const childData = (state = INITIAL_STATE, action)=>{
    switch (action.type) {
        case "UPDATE_CHILD_DATA":
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}
export default childData
