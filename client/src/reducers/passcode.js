
const passcode = (state = '', action)=>{
    switch (action.type) {
        case "UPDATE_PASSCODE":            
            return action.payload
        default:
            return state;
    }
}
export default passcode
