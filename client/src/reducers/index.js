import { combineReducers } from 'redux';
import authReducer from './authReducer';
import userReducer from './userReducer';
import childData from './childReducer';
import socketData from './sockets';
import parentData from './parentData';
import passcode from './passcode';

export default combineReducers({
  auth: authReducer,
  user: userReducer,
  childData,
  socketData,
  parentData,
  passcode
});
