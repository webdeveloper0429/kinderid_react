import { EMAIL_SUCCESS, INFO_UPDATE, USER_REGISTED, SET_ROUTE } from '../actions/types';

const INITIAL_STATE = {
  route: '',
  email: '',
  fullName: '',
  mobile: '',
  address: '',
  post: '',
  city: '',
  accessToken: '',
  shoppingRoute: '',
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case EMAIL_SUCCESS:
      return { ...state, route: action.payload.message, email: action.payload.email };
    case INFO_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case USER_REGISTED:
      return { ...state, accessToken: action.payload.token };
    case SET_ROUTE:
      return { ...state, shoppingRoute: action.payload };
    default:
      return state;
  }
}
