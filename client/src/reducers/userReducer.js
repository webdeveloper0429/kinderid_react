import {
  USER_FETCH_SUCCESS,
  UPDATE_CART,
  UPDATE_SUBSCRIPTION,
  UPDATE_TOTAL,
  SUBSCRIPTIONS_FETCH_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  parent: {},
  cart: {},
  sub: '',
  total: 249,
  subscriptions: [],
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case USER_FETCH_SUCCESS:
      return { ...state, parent: action.payload };
    case UPDATE_CART:
      return { ...state, cart: action.payload };
    case UPDATE_SUBSCRIPTION:
      return { ...state, sub: action.payload };
    case UPDATE_TOTAL:
      return { ...state, total: action.payload };
    case SUBSCRIPTIONS_FETCH_SUCCESS:
      return { ...state, subscriptions: action.payload.children };
    default:
      return state;
  }
}
