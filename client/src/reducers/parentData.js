const INITIAL_STATE = {
    childName : "",
    gaurdians : [],
    parentMobile : "",
    parentName : "",
    pushNotificationID : ""
};

const childData = (state = INITIAL_STATE, action)=>{
    switch (action.type) {
        case "UPDATE_PARENT_DATA":            
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}
export default childData
