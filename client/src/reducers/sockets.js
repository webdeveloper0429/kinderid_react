const initState = {
    endPoint: '',
    socket: '',
    mobileInitLocation:{
        lat: 0,
        lng: 0,
    }
}

const socketData = (state = initState, action)=>{
    switch(action.type){
        case 'REGISTER_SOCKET':
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}

export default socketData;