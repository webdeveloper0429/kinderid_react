import axios from 'axios';
import { EMAIL_SUCCESS, INFO_UPDATE, USER_REGISTED, SET_ROUTE } from './types';

export const checkEmail = email => async dispatch => {
  try {
    const { data } = await axios.post('https://api.kinder-id.com/auth/desktop/checkemail', { email });
    dispatch({ type: EMAIL_SUCCESS, payload: data });
  } catch (error) {
    console.log('error login', error);
  }
};

export const storeData = ({ prop, value }) => ({
  type: INFO_UPDATE,
  payload: { prop, value },
});

export const registerParent = user => async dispatch => {
  console.log('incoming user', user);
  try {
    const { data } = await axios.post('https://api.kinder-id.com/auth/desktop/registeruser', { user });
    dispatch({ type: USER_REGISTED, payload: data });
  } catch (error) {
    console.log('error login', error);
  }
};
export const setRoute = route => ({
  type: SET_ROUTE,
  payload: route,
});
