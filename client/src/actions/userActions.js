import axios from 'axios';
import {
  USER_FETCH_SUCCESS,
  UPDATE_CART,
  UPDATE_SUBSCRIPTION,
  UPDATE_TOTAL,
  SUBSCRIPTIONS_FETCH_SUCCESS,
} from './types';

export const fetchUserData = () => async dispatch => {
  const localToken = localStorage.getItem('kinderIdAccessToken');
  const token = JSON.parse(localToken);
  console.log('stored token', token);

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  try {
    const { data } = await axios.post('https://api.kinder-id.com/mobile/getuserdata', null, config);

    dispatch({ type: USER_FETCH_SUCCESS, payload: data });
  } catch (err) {
    console.log('error fetching user ', err);
    if (err.response.status === 502) {
      return console.log('network error');
      // dispatch({ type: CHILDREN_FETCH_ERROR, payload: "Network error, Please try again later" });
    }
    // dispatch({ type: CHILDREN_FETCH_ERROR, payload: err.response });
    // console.log('error', err);
  }
};

export const fetchSubscriptions = () => async dispatch => {
  const localToken = localStorage.getItem('kinderIdAccessToken');
  const token = JSON.parse(localToken);
  const config = {
    headers: { Authorization: `Bearer ${  token}` },
  };
  try {
    const { data } = await axios.post('https://api.kinder-id.com/mobile/subscriptions', null, config);

    dispatch({ type: SUBSCRIPTIONS_FETCH_SUCCESS, payload: data });
  } catch (err) {
    console.log('error: ', err.response);
    if (err.response.status === 502) {
      return console.log('network error');
      // dispatch({ type: GAURDIAN_FETCH_SUCCESS, payload: "Network error, Please try again later" });
    }
    // dispatch({ type: GAURDIAN_FETCH_SUCCESS, payload: err.response });
    console.log('error', err.respose);
  }
};

export const updateCart = cart => ({
  type: UPDATE_CART,
  payload: cart,
});
export const updateSub = sub => ({
  type: UPDATE_SUBSCRIPTION,
  payload: sub,
});
export const updateTotal = total => ({
  type: UPDATE_TOTAL,
  payload: total,
});
