export const EMAIL_SUCCESS = 'email_success';
export const INFO_UPDATE = 'info_update';
export const USER_REGISTED = 'user_registered';
export const SET_ROUTE = 'set_route';

export const USER_FETCH_SUCCESS = 'user_fetch_success';
export const UPDATE_CART = 'update_cart';
export const UPDATE_SUBSCRIPTION = 'update_subscription';
export const UPDATE_TOTAL = 'update_total';
export const SUBSCRIPTIONS_FETCH_SUCCESS = 'subscriptions_fetch_success';
