// import axios from 'axios';
// import {
//   FETCH_USER,
//   EMAIL_LOGIN
// } from './types';

// export const fetchUser = () => async dispatch => {
//     const res = await axios.get('/api/current_user');
//     dispatch({ type: FETCH_USER, payload: res.data });
// };

// export const emailLogin = email => async dispatch => {
//   const res = await axios.post('/auth/useremail', {email});
//   console.log(res.data);
//   dispatch({ type: EMAIL_LOGIN, payload: res.data });
// };

export const updateChildData = (payload)=>({
    type: 'UPDATE_CHILD_DATA',
    payload: payload
})
export const register_socket = (payload)=>({
    type: 'REGISTER_SOCKET',
    payload: payload
})
export const updateParentData = (payload)=>({
    type: 'UPDATE_PARENT_DATA',
    payload: payload
})

export const updatePasscode = (payload)=>({
    type: 'UPDATE_PASSCODE',
    payload: payload
})