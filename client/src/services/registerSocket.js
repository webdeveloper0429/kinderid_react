
import openSocket from 'socket.io-client';
import Config from '../config/config';

export const registerSocket = (component)=>{
    const socket = openSocket(Config.socketEndPoint, {reconnection: true});
    component.props.register_socket({
        endPoint: Config.socketEndPoint,
        socket: socket
    })
        
}