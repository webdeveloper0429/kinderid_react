import axios from 'axios';

export class ParentService {
    static BASE_URL = 'https://api.kinder-id.com/mobile/getpushnotificationid';
    static getData(wristband_code){
        return axios({
            method: 'post',
            url: '',
            headers: {'contentType': 'application/x-www-form-urlencoded'},
            data: {
                childWristband: wristband_code
            },
            baseURL: this.BASE_URL
        })
    }
}