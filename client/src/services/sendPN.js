import axios from "axios";

export class oneSignalService {
  static BASE_URL = "https://onesignal.com/api/v1/";
  static sendPN(pushIDs, childName) {
    return axios({
      method: "post",
      headers: {
        Authorization: "Basic MGJlOWVhNjktMzk0OS00ZTg4LWI4OWYtYWRmMzNhNDEwODIy",
        "Content-Type": "application/json"
      },
      url: "notifications",
      data: {
        app_id: "7df6e70b-b433-414b-953a-01d01a377089",
        // included_segments: ["All"],
        // data: { foo: "bar" },
        include_player_ids: pushIDs,
        contents: {
            en: `Your child ${childName} has been found.\nSwipe to establish contact.`
        },
      },
      baseURL: this.BASE_URL
    });
  }
}
