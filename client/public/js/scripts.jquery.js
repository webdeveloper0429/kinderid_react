$(function() {
                
    $("#mobilemenu").mmenu({
        extensions 	: [ "shadow-panels", "fx-panels-slide-100", "border-none", "theme-black", "fullscreen" ],
        navbars		: {
            title : "",
            add: false,
            content : [ "<div class='row'><div class='col-5'><img src='images/logo-white.png' /></div></div>", "close" ],
            height 	: 2
        },
        setSelected: false,
    }, { });
            
});