const express = require('express');
const http = require("http");
const sio  = require("socket.io");
const bodyParser = require('body-parser');

const cors = require('cors');
const cookieSession = require('cookie-session');
const keys = require('./config/keys');

const app = express();
const server = http.createServer(app);
const io = sio.listen(server);

const _ = require('lodash');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
    cookieSession({
        maxAge: 24 * 60 * 60 * 1000,
        keys: [keys.cookieSecret]
    })
);

app.use(express.static('client/build'));

const path = require('path');

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
});

const port = process.env.PORT || 5001;
server.listen(port, "0.0.0.0", () => {
    console.log("\n" + "**********************************");
    console.log("Server listening on port " + port);
    console.log("**********************************" + "\n");
});

const webList = {};
const mobileList = {};

io.on("connection", function(clientSocket) {
    clientSocket.on('disconnect', function(){
        console.log("disconnected", clientSocket.id);
        _.forEach(mobileList, function(mobile, wristband_code) {
            if(mobile.socketId === clientSocket.id){
                delete mobileList[wristband_code];
            }
        });
    });
    clientSocket.on("webUpdated", function(mobile_socket_id, webPosition, phoneNumber){
        try {
            io.to(mobile_socket_id)
            .emit("locationUpdateFromWeb", webPosition, phoneNumber);
        } catch (ex) {}
    });

    clientSocket.on("mobileUpdated", function(web_socket_id, mobilePosition) {
        try {
            io.to(web_socket_id)
            .emit(
                "locationUpdateFromMobile",
                mobilePosition
            );
        } catch (ex) {}
    });

    clientSocket.on("searchMobile", function(wristband_code, webInitLocation) {
        console.log(`web searching *${wristband_code}*'s parnet`);
        // webList[wristband_code] = {
        //     socketId: clientSocket.id,
        //     wristband_code: wristband_code
        // };
        try {
            if(mobileList[wristband_code].busy){
                io.to(clientSocket.id)
                .emit("mobileIsBusy", wristband_code);
            }
            else{
                io.to(mobileList[wristband_code].socketId)
                .emit("searchMobile", clientSocket.id, mobileList[wristband_code].socketId, webInitLocation, wristband_code);
            }
        } catch (ex) {
            try {
                io.to(clientSocket.id)
                .emit("failedConnection", wristband_code);
            } catch (ex) {}
        }
    });
    clientSocket.on("searchGuardian", function(gaurdianRefId, childData, webInitLocation) {
        console.log(`web searching *${childData.wristband_code}*'s guardian`);
        let guardian_socketId ;
        _.forEach(mobileList, function(mobile, wristband_code) {
            if(mobile.user_id === gaurdianRefId){
                guardian_socketId = mobile.socketId;
            }
        });
        if(guardian_socketId){
            try {
                io.to(guardian_socketId)
                .emit("searchGuardian", clientSocket.id, guardian_socketId, webInitLocation, childData);
            } catch (ex) {
                try {
                    io.to(clientSocket.id)
                    .emit("failedGuardian_"+gaurdianRefId);
                } catch (ex) {}
            }
        }
        else{
            try {
                io.to(clientSocket.id)
                .emit("failedGuardian_"+gaurdianRefId);
            } catch (ex) {}
        }
    });
    clientSocket.on("guardianConnectedParent", function(wristband_code) {
        try {
            if(mobileList[wristband_code].busy){
                io.to(clientSocket.id)
                .emit("mobileIsBusy", wristband_code);
            }
            else{
                io.to(mobileList[wristband_code].socketId)
                .emit("someoneConnected");
            }
        } catch (ex) {
        }
    });
    clientSocket.on("guardianConnectedGuardian", function(gaurdianRefId) {
        let guardian_socketId ;
        _.forEach(mobileList, function(mobile, wristband_code) {
            if(mobile.user_id === gaurdianRefId){
                guardian_socketId = mobile.socketId;
            }
        });
        if(guardian_socketId){
            try {
                io.to(guardian_socketId)
                .emit("someoneConnected");
            } catch (ex) {
            }
        }
    });

    clientSocket.on("mobileYes", function(web_socket_id, mobile_socket_id, mobileInitLocation, children, user_id) {
        children.map((child)=>{
            mobileList[child.wristband].busy = true
        })
        try {
            io.to(web_socket_id)
            .emit("acceptedConnection", web_socket_id, mobile_socket_id, mobileInitLocation, user_id);
        } catch (ex) {}
    });

    clientSocket.on("mobileNo", function(web_socket_id) {
        try {
            io.to(web_socket_id)
            .emit("declinedConnection");
        } catch (ex) {}
    });

    clientSocket.on("mobileOn", function(wristband_code, user_id) {
        var message = "Phone " + wristband_code + " was connected.";
        console.log(message);
        mobileList[wristband_code] = {
            socketId: clientSocket.id,
            wristband_code: wristband_code,
            user_id: user_id,
            busy: false
        };
    });

    clientSocket.on("reunited_success", function(web_socket_id, children) {
        console.log("reunited_success");
        children.map((child)=>{
            mobileList[child.wristband].busy = false;
        })
        try {
            io.to(web_socket_id).emit("reunited_success");
        } catch (ex) {}
    });
    clientSocket.on("reunited_failed", function(web_socket_id, children) {
        console.log("reunited_failed");
        children.map((child)=>{
            mobileList[child.wristband].busy = false;
        })
        try {
            io.to(web_socket_id).emit("reunited_failed");
        } catch (ex) {}
    });
});
